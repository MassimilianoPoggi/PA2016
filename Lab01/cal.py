import calendar, datetime

year = datetime.datetime.now().year + 1
while True:
	if calendar.isleap(year):
		print("Next leap year: {0}".format(year))
		break
	year += 1

print("Leap years between 2000 and 2050: {0}".format(calendar.leapdays(2000, 2051)))

print("Jule 29, 2016 will be on a {0}".format(calendar.day_name[calendar.weekday(2016, 7, 29)]))
