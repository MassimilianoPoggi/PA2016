alkaline_earth_metals = [("Barium", 56), ("Berillium", 4), ("Calcium", 20), ("Magnesium", 12), ("Radium", 88), ("Strontium", 38)]

print("Highest atomic number is {0}".format(max(alkaline_earth_metals, key=lambda x: x[1])))
alkaline_earth_metals.sort(key=lambda x: x[1])
print("Sorted metals: {0}".format(alkaline_earth_metals))

alkaline_earth_metals = {key:value for (key,value) in alkaline_earth_metals}
print("Metals dictionary: {0}".format(alkaline_earth_metals))

noble_gases = {"Helium": 2, "Neon": 10, "Argon": 18, "Krypton": 36, "Xenon": 54, "Radon": 86}
print("Gases dictionary: {0}".format(noble_gases))

elements = alkaline_earth_metals.copy()
elements.update(noble_gases)
print("Elements: {0}".format(elements))

elements_sorted = sorted([(key, value) for key,value in elements.items()], key=lambda x: x[1])
print("Sorted elements: {0}".format(elements_sorted))


