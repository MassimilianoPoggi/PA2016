def identity(size):
	return [[0 if j != i else 1 for j in range(size)] for i in range(size)]

def square(size):
	return [[i*size + j for j in range(size)] for i in range(size)]

def transpose(matrix):
	return [[x[i] for x in matrix] for i in range(len(matrix[0]))]

def multiply(matrix1, matrix2):
	return [[sum(map(lambda x: x[0]*x[1], zip(row1, col2))) for col2 in transpose(matrix2)] for row1 in matrix1]
