import shell, sys, os

def cat(args):
	for filename in args:
		try:
			with open(filename, 'r') as file:
				for line in file:
					sys.stdout.write(line)
		except OSError as e:
			sys.stderr.write("cat: {0}: {1}\n".format(filename, e.strerror))

def chmod(args):
	try:
		os.chmod(args[1], int(args[0], 8))
	except OSError as e:
		sys.stderr.write("chmod: cannot access '{0}': {1}\n".format(args[1], e.strerror))

def more(args):
	for filename in args:
		try:
			with open(filename, 'r') as file:
				if (len(args) > 1):
					sys.stdout.write("::::::::::::::\n{0}\n::::::::::::::\n".format(filename))

				count = 0
				for line in file:
					sys.stdout.write(line)
					count += 1
					if (count == 30):
						input()
						count = 0

		except FileNotFoundError:
			sys.stderr.write("more: stat of {0} failed: No such file or directory\n".format(filename))
		except IsADirectoryError:
			sys.stdout.write("*** {0}: directory ***\n".format(filename))

if __name__ == '__main__':
	args = sys.argv[1:]
	getattr(shell, args[0])(args[1:])
