scales = { "Celsius": (0, 100, "C"),
		"Fahrenheit": (32, 212, "F"),
		"Kelvin": (273.15, 373.15, "K"),
		"Rankine": (492, 672, "Ra"),
		"Delisle": (150, 0, "D"),
		"Rømer": (7.5, 60, "Rø"),
		"Newton": (0, 33, "N"),
		"Réaumur": (0, 80, "Ré") }

def convert(val, scale_from, scale_to):
	scale1 = scales[scale_from][1] - scales[scale_from][0]
	scale2 = scales[scale_to][1] - scales[scale_to][0]
	return (val - scales[scale_from][0]) * scale2 / scale1 + scales[scale_to][0]

def table(value):
	values = {s1: {s2: convert(value, s1, s2) for s2 in scales.keys()} for s1 in scales.keys()}

	column_lengths = [max([len("{0:.2f}".format(values[k1][k2])) for k1 in scales.keys()] + [len(k2)]) for k2 in sorted(scales.keys())]
	title_length = max([len(k) for k in scales.keys()])

	result = " | ".join(["{{:^{}}}"] * (len(values) + 1)).format(title_length, *column_lengths).format(" "*title_length, *sorted(scales.keys()))
	result += "\n"
	for scale in sorted(scales.keys()):
		result += " | ".join(["{{:{}}}"] + ["{{:{}.2f}}"] * len(values)).format(title_length, *column_lengths) \
				.format(scale, *[values[scale][to] for to in sorted(scales.keys())])+ "\n"

	return result


def toAll(value, scale_from):
	return "Conversioni ordinate: " + " | ".join(map(lambda x: "{0[0]} °{0[1]}".format(x),
		sorted([(convert(value, scale_from, key), val[2]) for key,val in scales.items()])))

if __name__ == '__main__':
	print(table(50))
