def pi_series():
	i, res, sign = 1, 4, 1
	while True:
		yield res
		i += 2
		sign *= -1
		res += sign * (4 / i)

def e_series():
	def fact():
		i, res = 1, 1
		while True:
			yield res
			i += 1
			res *= i
	res = 1
	for f in fact():
		yield res
		res += 1/f

def euler_accelerator(gen):
	s0, s1, s2 = next(gen), next(gen), next(gen)
	while True:
		yield s2 - (s2 - s1)**2 / (s0 - 2*s1 + s2)
		s0, s1, s2 = s1, s2, next(gen)
