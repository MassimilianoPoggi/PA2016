from random import randint
from functools import reduce

def trial_divison(n):
	return len([x for x in range(2, int(n**0.5)+1) if n % x == 0]) == 0

def lucaslehmer(n):
	def s(i):
		return 4 if i == 0 else s(i-1)**2 - 2

	n = n+1
	if bin(n).count('1') != 1:
		return False

	count = 0
	while n > 0:
		count += 1
		n >>= 1

	return s(count - 2) % (n - 1) == 0

def littlefermat(n):
	"""Uses 10 random values for A"""
	As = [randint(1, n-1) for i in range(10)]
	return reduce(lambda x, y: x and y, map(lambda x: pow(x, n-1, n) == 1, As), True)

def is_prime(n):
	return trial_division(n) if n <= 10000 else lucaslehmer(n) if n <= 524280 else littlefermat(n)

if __name__ == '__main__':
	print(is_prime(2**31-1))
