print("Sum of divisors of 3 and 5 below 1000: {}".format(sum([x for x in range(1000) if x % 3 == 0 or x % 5 == 0])))

print("Sum of digits of 2**1000: {}".format(sum([int(x) for x in str(2**1000)])))

def fib():
	a,b = 0,1
	while True:
		yield a
		a,b = b,a+b

ris = fib()
i = next(ris)
while (i // 10**999 == 0):
	i = next(ris)
print("Fib over 1000 digits: {}".format(i))

def expand_prime(n, max):
	i = n
	while (i*n < max):
		i = i*n
	return i

from functools import reduce
def divisors(n):
	return [x for x in range(2,int(n**0.5)+1) if n % x == 0]
def primes(max):
	return [x for x in range(2,max) if len(divisors(x)) == 0]

print("LCM for numbers between 1 and 20: {}".format(reduce(lambda a,b: a*b, map(lambda x: expand_prime(x, 20), primes(20)))))
