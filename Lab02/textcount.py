from collections import Counter
import re

def worditerator(filename):
	with open(filename, 'r') as f:
		for line in f:
			for word in line.split(None):
				yield word

def punctfilter(reader):
	for fragment in reader:
		for word in re.split("\W+", fragment):
			yield word

def freqs(filename, min):
	c = Counter()
	for word in punctfilter(worditerator(filename)):
		if word != '':
			c[word] += 1

	return sorted(filter(lambda x: x[1] >= min, [(word, freq) for word, freq in c.items()]), key=lambda x: x[1], reverse=True)

if __name__ == '__main__':
	print(freqs("textcount.py", 2))
