
from functools import reduce
from math import sin

def fact(start = 1, step = 1):
	n, res = start, reduce(lambda x,y: x*y, range(1, start + 1),1)
	while(True):
		yield res
		res *= reduce(lambda x,y: x*y, range(n + 1, n + step + 1))
		n = n + step

def mysin(x,n):
	f = fact(step = 2)
	return sum([(-1)**(n+1) * x**(2*n-1) / f for (n,f) in zip(range(1, n+1), f)])

if __name__ == '__main__':
	approx = int(input("Insert approx level: "))
	[print("{:.2f} rad: mysin = {:.5f} sin = {:.5f} error = {:.8}".format(x, mysin(x, approx), sin(x), mysin(x, approx) - sin(x)))
		for x in map(lambda x: x/100, range(0, 314*2))]
